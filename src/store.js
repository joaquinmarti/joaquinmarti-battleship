const coordsToKey = require('./helpers/coords-to-key');
const randomBool = require('./helpers/random-bool');
const randomCoords = require('./helpers/random-coords');
const calcShipPosition = require('./helpers/calc-ship-position');
const doesOverlap = require('./helpers/does-overlap');
const resolveHit = require('./helpers/resolve-hit');
const RESULTS = require('./const/results');

//
const shoots = new Map();
const targets = new Map();

//
const addShoot = (target) => {
  const key = coordsToKey(target);

  // Avoid repeating shoots
  if (shoots.get(key)) {
    return RESULTS.repeated;
  }

  // Check what ship was hit
  const hitID = targets.get(key);

  // Remove the shoot from the targets
  if (hitID) {
    targets.delete(key);
  }

  // Result needs to be calculated after the targets.delete(key) line
  // as it involves checking if a ship is sunk
  const result = resolveHit(hitID, [...targets.values()]);

  shoots.set(key, result);

  // No more ships to target
  if (targets.size === 0) {
    return RESULTS.youWon;
  }

  return result;
};

const saveCoordinates = (coordinates, shipId) => {
  coordinates.forEach((coord) => {
    targets.set(coordsToKey(coord), shipId);
  });
};

const prepareTargets = (gridSize, ships) => {
  const placeShip = (ship, shipId) => {
    const isHorizontal = randomBool();

    // Generates initial random coords and calculate all
    const initialPosition = randomCoords(gridSize, isHorizontal, ship);
    const coordinates = calcShipPosition(initialPosition, isHorizontal, ship);

    // If ship overlaps previous ones, recalculate, otherwise save coordinates
    if (doesOverlap(coordinates, targets)) {
      placeShip(ship, shipId);
    } else {
      saveCoordinates(coordinates, shipId);
    }
  }

  //
  ships.forEach((ship, n) => {
    placeShip(ship, n + 1);
  });
};

module.exports = {
  addShoot,
  shoots,
  targets,
  prepareTargets,
};