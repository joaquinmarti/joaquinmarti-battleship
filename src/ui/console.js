const readline = require('readline');
const RESULTS = require('../const/results');
const shootToKey = require('../helpers/coords-to-key');
const content = require('../content/en.json');

const print = (line) => console.log(line);

//
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.on('close', function () {
  print(content.close);
  process.exit(0);
});

//
const welcome = () => print(content.welcome);
const command = (callback) => rl.question(content.command, callback);
const exit = () => rl.close();
const help = () =>  print(content.help);
const error = () => print(content.error);

const feedback = (result) => {
  switch (result) {
    case RESULTS.hits:
      print(content.hits);
      break;
    case RESULTS.sinks:
      print(content.sinks);
      break;
    case RESULTS.misses:
      print(content.misses);
      break;
    case RESULTS.repeated:
      print(content.repeated);
      break;
    case RESULTS.youWon:
      print(content.youWon);
      break;
  }
};

const printGrid = (display, gridSize) => {
  let output = '\n';

  for (let r = 0; r < gridSize; r++) {
    output += '|';

    for (let c = 0; c < gridSize; c++) {
      let result = display.get(shootToKey([r, c])) || ' ';
      output += ` ${result} `;
      output += '|';
    }

    output += '\n';
  }

  print(output);
};

module.exports = {
  welcome,
  command,
  exit,
  help,
  error,
  feedback,
  printGrid,
};