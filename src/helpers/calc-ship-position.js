module.exports = (position, isHorizontal, ship) => {
  return Array(ship.size).fill(undefined).map((e, i) => {
    if (isHorizontal) {
      return [position[0], position[1] + i];
    } else {
      return [position[0] + i, position[1]];
    }
  });
};