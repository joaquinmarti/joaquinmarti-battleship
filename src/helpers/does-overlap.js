const coordsToKey = require('./coords-to-key');

module.exports = (coordinates, targets) => {
  return coordinates.reduce((found, coord) => {
    return targets.has(coordsToKey(coord)) || found;
  }, false);
};