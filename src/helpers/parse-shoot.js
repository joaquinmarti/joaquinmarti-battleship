const letterToInt = require('./letter-to-int');

module.exports = (coords, gridSize) => {
  const r = letterToInt(coords.charAt(0));
  const c = parseInt(coords.substring(1));

  if (r > gridSize || r < 1 || isNaN(r) || c > gridSize || c < 1 || isNaN(c)) {
    return null;
  }

  return [r - 1, c - 1]; // -1 to convert the number to an index
};