const RESULTS = require('../const/results');

module.exports = (hitID, targets) => {
  if (!hitID) {
    return RESULTS.misses;
  }

  if (targets.find(id => id === hitID)) {
    return RESULTS.hits;
  }

  return RESULTS.sinks;
}