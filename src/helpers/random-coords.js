const randomNumber = require('./random-number');

module.exports = (gridSize, isHorizontal, ship) => {
  const availableRows = gridSize - (!isHorizontal ? ship.size : 0);
  const availableColumns = gridSize - (isHorizontal ? ship.size : 0);

  return [
    randomNumber(availableRows),
    randomNumber(availableColumns)
  ];
};
