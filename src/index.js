const { gridSize, ships } = require('../battleships.config');
const { welcome, command, exit, help, error, feedback, printGrid } = require('./ui/console');
const { addShoot, prepareTargets, shoots, targets  } = require('./store');
const parseShoot = require('./helpers/parse-shoot');
const RESULTS = require('./const/results');

//
const askForCommand = () => {
  command((input) => {
    const availableCommands = {
      exit: () => exit(),
      help: () => help(),
      periscope: () => printGrid(shoots, gridSize),
      airforce: () => printGrid(new Map([...targets].concat([...shoots])), gridSize),
    };

    (availableCommands[input] || (() => {
      const coords = parseShoot(input, gridSize);

      if (coords) {
        const result = addShoot(coords);

        feedback(result);
        printGrid(shoots, gridSize);

        // Finishes the game
        if (result === RESULTS.youWon) {
          exit();
        }

      } else {
        error();
      }
    }))();

    askForCommand();
  });
}

prepareTargets(gridSize, ships);
welcome();
askForCommand();