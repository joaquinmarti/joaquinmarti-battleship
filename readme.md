# Joaquín Martí's assignment

This repo contains a Battleships game written in Javascript.

## Run the game

```
node src/index.js
```

## Docs

As requested in the description, to keep simplicity I have decided to write the Battleships game in Node, avoiding the overhead of a major Javascript framework and the complexity of styling the UI in a browser.

### Approach

The code is separated in few layers, each one of them with a clear purpose:

- `store`. Manages the internal state of the app.
- `ui`. It is in responsible of rendering the interface. In this case there is just a UI method to log in the console.
- `helpers`. A set of helper functions.
- `content`. Content literals.
- `const`.  Some needed constants.

### The store

The store keeps the state of the app and has functionality to change it, in a pseudo-flux approach.

To keep simplicity, the data model is reduced to two objects: a list of targets and a list of shoots.

The targets are the coordinates of the ships. The shoots are the collection of coordinates selected by the user.

Both are implemented as a Javascript map:

- In the case of the shoots, it was important to have an enumerable data set, so a back and forth mechanism could be implemented as a "time machine".
- In the case of the targets, being able to check the size of the data set was important to control when all the target are hit.
- In both cases, it is needed to have a value associated to the coordinates: a numeric id of this ship in case of the targets and the result (hit, sinks, misses) of the shoots.
- Having both models implemented with the same data set makes it easy and clear to concat them to build the `airforce` feature. With that command the user can cheat and find the ships.

I have discarded other approaches like keeping a data set for the grid to avoid data duplicity in the "time machine" feature.

The store also has a couple of functions to manipulate the store data. Those functionsn include some business logic which probably should not be there in a larger application.

The "prepare targets" function places randomly the ships on the board avoiding overlaps. Also to keep simplicity I have used recursivity to avoid overlapping: if a ship overlaps a previous one, recalculate the position. A better algorithm could produce a more performant function in terms of big O notation, but for the purposes of this game, the recursive function does the job and it is easy to understand.